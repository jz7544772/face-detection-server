package com.jz7544772.facedetection.payload;

public class ErrorResponsePayload {
    private String message;

    public ErrorResponsePayload(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
