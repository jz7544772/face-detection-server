package com.jz7544772.facedetection.payload;

import com.fasterxml.jackson.annotation.JsonProperty;

public class FaceDetectionRequestPayload {

    @JsonProperty(value="image_url")
    private String imageUrl;

    @JsonProperty(value="return_attributes")
    private String returnAttributes;

    public String getReturnAttributes() {
        return returnAttributes;
    }
    public void setReturnAttributes(String returnAttributes) {
        this.returnAttributes = returnAttributes;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
    public String getImageUrl() {
        return this.imageUrl;
    }

}
