package com.jz7544772.facedetection.payload;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.HashMap;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class FaceDetectionResponsePayload {

    @JsonProperty(value="request_id")
    private String requestId;
    @JsonProperty(value="image_id")
    private String imageId;
    private List<FaceDetectionResponsePayload.Face> faces;

    public List<FaceDetectionResponsePayload.Face> getFaces() { return this.faces; }

    @JsonIgnoreProperties(ignoreUnknown = true)
    private static class Face {
        @JsonProperty(value="face_rectangle")
        private FaceDetectionResponsePayload.FaceRectangle faceRectangle;
        private FaceDetectionResponsePayload.Attributes attributes;

        public FaceDetectionResponsePayload.Attributes getAttributes() { return this.attributes; }
        public FaceDetectionResponsePayload.FaceRectangle getFaceRectangle() { return this.faceRectangle; }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    private static class FaceRectangle {
        private int top;
        private int left;
        private int width;
        private int height;

        public int getTop() {
            return top;
        }

        public int getLeft() {
            return left;
        }

        public int getWidth() {
            return width;
        }

        public int getHeight() {
            return height;
        }
    }

    @JsonIgnoreProperties(ignoreUnknown = true)
    private static class Attributes {
        private HashMap<String, Float> headpose;

        public HashMap<String, Float> getHeadpose() {
            return this.headpose;
        }
    }
}
