package com.jz7544772.facedetection.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.jz7544772.facedetection.exception.NoFaceDetectedException;
import com.jz7544772.facedetection.payload.FaceDetectionRequestPayload;
import com.jz7544772.facedetection.payload.FaceDetectionResponsePayload;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponentsBuilder;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
public class FaceDetectionController {

    private static final String apiKey = "ffa1Bz2DK6zZSglYacTNPKBZ6bkmthN_";
    private static final String apiSecret = "uaBjg_wkgyRuaB4W1nyqf6JVPRKT72OV";
    private static final String requestUrl = "https://api-us.faceplusplus.com/facepp/v3/detect";

    @RequestMapping(value = "api/face-detection", method = RequestMethod.POST)
    public FaceDetectionResponsePayload faceDetectionPost(@RequestBody FaceDetectionRequestPayload payload) throws JsonProcessingException, NoFaceDetectedException {
        // Request Headers
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_JSON);

        // Request Params
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(requestUrl)
                .queryParam("api_key", apiKey)
                .queryParam("api_secret", apiSecret);

        // Request Body
        HttpEntity<FaceDetectionRequestPayload> requestEntity = new HttpEntity<>(payload, headers);

        // Do POST Request through Rest Client
        RestTemplate restClient = new RestTemplate();
        HttpEntity<FaceDetectionResponsePayload> response = restClient.exchange(
                builder.toUriString(),
                HttpMethod.POST,
                requestEntity,
                FaceDetectionResponsePayload.class
        );

        FaceDetectionResponsePayload responsePayload = response.getBody();

        if (responsePayload.getFaces().size() == 0) {
            throw new NoFaceDetectedException();
        }

        return responsePayload;
    }

    @ExceptionHandler({NoFaceDetectedException.class})
    void handleNoFaceDetected(HttpServletResponse response) throws IOException {
        response.sendError(HttpStatus.BAD_REQUEST.value(), "No face detected");
    }
}
